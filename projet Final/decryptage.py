# -*- coding: utf-8 -*-
"""
Created on Sun May  1 17:48:12 2022

@author: Cotinus
"""

def crypto1(phrase):
    code = {'a': '01', 'b': '02', 'c': '03', 'd': '04', 'e': '05',
            'f': '06', 'g': '07', 'h': '08', 'i': '09', 'j': '10',
            'k': '11', 'l': '12', 'm': '13', 'n': '14', 'o': '15',
            'p': '16', 'q': '17', 'r': '18', 's': '19', 't': '20',
            'u': '21', 'v': '22', 'w': '23', 'x': '24', 'y': '25',
            'z': '26', ' ': '27'}
            
    reponse = []
    for lettre in phrase:
        reponse.append(code[lettre])
                
    reponseFinal = ''
    for nombre in reponse:
        reponseFinal += nombre
            
    return reponseFinal

def crypto2(phrase, decalage):
    code1 = {'a': '01', 'b': '02', 'c': '03', 'd': '04', 'e': '05',
             'f': '06', 'g': '07', 'h': '08', 'i': '09', 'j': '10',
             'k': '11', 'l': '12', 'm': '13', 'n': '14', 'o': '15',
             'p': '16', 'q': '17', 'r': '18', 's': '19', 't': '20',
             'u': '21', 'v': '22', 'w': '23', 'x': '24', 'y': '25',
             'z': '26', ' ': '27'}
    code2 = {1: 'a', 2: 'b', 3: 'c', 4: 'd', 5: 'e',
             6: 'f', 7: 'g', 8: 'h', 9: 'i', 10: 'j',
             11: 'k', 12: 'l', 13: 'm', 14: 'n', 15: 'o',
             16: 'p', 17: 'q', 18: 'r', 19: 's', 20: 't',
             21: 'u', 22: 'v', 23: 'w', 24: 'x', 25: 'y',
             26: 'z', 27: ' '}
    
    decimal = []
    for lettre in phrase:
        decimal.append(int(code1[lettre]))
        
    for i in range(len(decimal)):
        decimal[i] = decimal[i] + decalage
        if decimal[i] > 27:
            decimal[i] = decimal[i] - 27
    
    reponseFinal = ''
    for nombre in decimal:
        reponseFinal += code2[nombre]
    
    return reponseFinal
    

def choixcrpyptage(phrase, numeros, cle = None):
    phrasecrypte = ''
    if numeros == 1:
        phrasecrypte = crypto1(phrase)
        
    if numeros == 2:
        phrasecrypte = crypto2(phrase, cle)
        
    return phrasecrypte
        

def ihm():
    print(' ! A tout moment, tapez 0 pour sortir du programme !')
    phrase = input('Tapez la phrase ou le mot que vous voulez crypter.\n')
    reponse = int(input('Voulez vous faire du cyptage(1) ou du décryptages(2) ?\n'))
    if reponse == 1:
        typecryptage = int(input('Voulez vous utiliser un cryptage numerique(a = 1) [1], ou un cryptage César (a = c pour 2 de décalage) [2] ?\n'))
        if typecryptage in [1]:
            return choixcrpyptage(phrase, typecryptage)
        elif typecryptage == 2:
            decalage = input('Quel décalage entre les lettres voulez vous avoir ?\n')
            return choixcrpyptage(phrase, typecryptage, decalage)
    elif reponse == 2:
        typecryptage = int(input('Voulez vous décrypter un cryptage numerique(a = 1) [1], ou un cryptage César (a = c pour 2 de décalage) [2] ?\n'))
    
    return None
        



print(ihm())



    # code = {['a', '01'], ['b', '02'], ['c', '03'], ['d', '04'], ['e', '05'],
    #         ['f', '06'], ['g', '07'], ['h', '08'], ['i', '09'], ['j', '10'],
    #         ['k', '11'], ['l', '12'], ['m', '13'], ['n', '14'], ['o', '15'],
    #         ['p', '16'], ['q', '17'], ['r', '18'], ['s', '19'], ['t', '20'],
    #         ['u', '21'], ['v', '22'], ['w', '23'], ['x', '24'], ['y', '25'],
    #         ['z', '26'], [' ', '27']}